__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.conf.urls import patterns, include, url


urlpatterns = patterns('news.views',
    url(r'^blog/', include('zinnia.urls')),
)
