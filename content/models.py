__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class Students(TimeStampedModel):
    name = models.CharField(_('name'), max_length=255)
    description = models.TextField(_('description'), blank=True, default='')
    sort = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('sort',)

    def __unicode__(self):
        return self.name


class Parents(TimeStampedModel):
    name = models.CharField(_('name'), max_length=255)
    description = models.TextField(_('description'), blank=True, default='')
    sort = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('sort',)

    def __unicode__(self):
        return self.name


class Projects(TimeStampedModel):
    name = models.CharField(_('name'), max_length=255)
    description = models.TextField(_('description'), blank=True, default='')
    sort = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('sort',)

    def __unicode__(self):
        return self.name


class Outs(TimeStampedModel):
    name = models.CharField(_('name'), max_length=255)
    description = models.TextField(_('description'), blank=True, default='')
    sort = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('sort',)

    def __unicode__(self):
        return self.name


class Video(TimeStampedModel):
    name = models.CharField(_('name'), max_length=255)
    code = models.TextField(_('embeded code'), blank=True, default='')

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return self.name