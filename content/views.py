import json
import pytz
import datetime

from django.http import HttpResponse
from schedule.models import Calendar
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404

from content.models import (
    Parents,
    Projects,
    Students,
    Outs
)


class CouncilPage(TemplateView):
    template_name = 'content/council.html'


class CalendarPage(TemplateView):
    template_name = 'content/calendar.html'


class StudentsPage(TemplateView):
    template_name = 'content/items.html'

    def get_context_data(self, **kwargs):
        context = super(StudentsPage, self).get_context_data(**kwargs)
        context['items'] = Students.objects.all()
        return context


class ParentsPage(TemplateView):
    template_name = 'content/items.html'

    def get_context_data(self, **kwargs):
        context = super(ParentsPage, self).get_context_data(**kwargs)
        context['items'] = Parents.objects.all()
        return context


class ProjectsPage(TemplateView):
    template_name = 'content/items.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectsPage, self).get_context_data(**kwargs)
        context['items'] = Projects.objects.all()
        return context


class OutsPage(TemplateView):
    template_name = 'content/items.html'

    def get_context_data(self, **kwargs):
        context = super(OutsPage, self).get_context_data(**kwargs)
        context['items'] = Outs.objects.all()
        return context


def api_occurrences_custom(request):
    utc=pytz.UTC
    # version 2 of full calendar
    if '-' in request.GET.get('start'):
        convert = lambda d: datetime.datetime.strptime(d, '%Y-%m-%d')
    else:
        convert = lambda d: datetime.datetime.utcfromtimestamp(float(d))
    start = utc.localize(convert(request.GET.get('start')))
    end = utc.localize(convert(request.GET.get('end')))
    calendar = get_object_or_404(Calendar, slug=request.GET.get('calendar_slug'))
    response_data =[]
    for event in calendar.events.filter(start__gte=start, end__lte=end):
        occurrences = event.get_occurrences(start, end)
        for occurrence in occurrences:
            response_data.append({
                "id": occurrence.id,
                "title": occurrence.title,
                "start": occurrence.start.isoformat(),
                "end": occurrence.end.isoformat(),
                "description": event.description
            })
    return HttpResponse(json.dumps(response_data), content_type="application/json")
