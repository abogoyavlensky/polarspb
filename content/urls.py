__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.conf.urls import patterns, include, url

from views import (
    CouncilPage,
    CalendarPage,
    StudentsPage,
    ParentsPage,
    ProjectsPage,
    OutsPage,
    api_occurrences_custom
)

urlpatterns = patterns('content.views',
    url(r'^council/', CouncilPage.as_view(), name='council_page'),
    url(r'^calendar/', CalendarPage.as_view(), name='calendar_page'),
    url(r'^students/', StudentsPage.as_view(), name='students_page'),
    url(r'^parents/', ParentsPage.as_view(), name='parents_page'),
    url(r'^projects/', ProjectsPage.as_view(), name='projects_page'),
    url(r'^outs/', OutsPage.as_view(), name='outs_page'),
    url(r'^api/occurrences/custom', api_occurrences_custom, name='api_occurences_custom'),
)
