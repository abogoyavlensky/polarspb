__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.contrib import admin
from django.forms import ModelForm
from suit_redactor.widgets import RedactorWidget
from .models import Students, Parents, Projects, Outs, Video


class PageForm(ModelForm):
    class Meta:
        widgets = {
            'description': RedactorWidget(editor_options={'lang': 'ru'})
        }


class StudentsAdmin(admin.ModelAdmin):
    form = PageForm
    list_display = ('name', 'description', 'sort')


class ParentsAdmin(admin.ModelAdmin):
    form = PageForm
    list_display = ('name', 'description', 'sort')


class ProjectsAdmin(admin.ModelAdmin):
    form = PageForm
    list_display = ('name', 'description', 'sort')


class OutsAdmin(admin.ModelAdmin):
    form = PageForm
    list_display = ('name', 'description', 'sort')


class VideoAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')

admin.site.register(Parents, ParentsAdmin)
admin.site.register(Students, StudentsAdmin)
admin.site.register(Projects, ProjectsAdmin)
admin.site.register(Outs, OutsAdmin)
admin.site.register(Video, VideoAdmin)
