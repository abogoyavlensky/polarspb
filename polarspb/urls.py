from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^content/', include('content.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^$', include('main.urls')),
    url(r'^schedule/', include('schedule.urls')),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)