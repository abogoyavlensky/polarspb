from django.views.generic import TemplateView

from content.models import Video


class LandingPage(TemplateView):
    template_name = 'main/main.html'

    def get_context_data(self, **kwargs):
        context = super(LandingPage, self).get_context_data(**kwargs)
        try:
            context['latest_video'] = Video.objects.latest('created')
        except Video.DoesNotExist:
            context['latest_video'] = ''
        return context